import * as React from 'react'
import { FaZhihu } from '@react-icons/all-files/fa/FaZhihu'
import { FaGithub } from '@react-icons/all-files/fa/FaGithub'
import { IoSunnyOutline } from '@react-icons/all-files/io5/IoSunnyOutline'
import { IoMoonSharp } from '@react-icons/all-files/io5/IoMoonSharp'

import { useDarkMode } from 'lib/use-dark-mode'
import * as config from 'lib/config'

import styles from './styles.module.css'

// TODO: merge the data and icons from PageSocial with the social links in Footer

export const FooterImpl: React.FC = () => {
  const [hasMounted, setHasMounted] = React.useState(false)
  const { isDarkMode, toggleDarkMode } = useDarkMode()

  const onToggleDarkMode = React.useCallback(
    (e) => {
      e.preventDefault()
      toggleDarkMode()
    },
    [toggleDarkMode]
  )

  React.useEffect(() => {
    setHasMounted(true)
  }, [])

  return (
    <footer className={styles.footer}>
      <div className={styles.copyright}>Copyright 2022 Hubfox</div>

      <div className={styles.settings}>
        {hasMounted && (
          <a
            className={styles.toggleDarkMode}
            href='#'
            role='button'
            onClick={onToggleDarkMode}
            title='Toggle dark mode'
          >
            {isDarkMode ? <IoMoonSharp /> : <IoSunnyOutline />}
          </a>
        )}
      </div>

      <div className={styles.social}>
        {config.twitter && (
          <a
            className={styles.twitter}
            href={`https://twitter.com/${config.twitter}`}
            title={`Twitter @${config.twitter}`}
            target='_blank'
            rel='noopener noreferrer'
          >
            
            here 
          </a>
        )}

        {config.zhihu && (
          <a
            className={styles.zhihu}
            href={`https://zhihu.com/people/${config.zhihu}`}
            title={`Zhihu @${config.zhihu}`}
            target='_blank'
            rel='noopener noreferrer'
          >
            <FaZhihu />
          </a>
        )}

        {config.github && (
          <a
            className={styles.github}
            href={`https://instagram.com/${config.github}`}
            title={`Instagram @${config.github}`}
            target='_blank'
            rel='noopener noreferrer'
          >
            
            <FaGithub />
                   </a>
        )}

        {config.linkedin && (
          <a
            className={styles.linkedin}
            href={`https://www.tiktok.com/@${config.linkedin}`}
            title={`Tik Tok ${config.linkedin}`}
            target='_blank'
            rel='noopener noreferrer'
          >
            

            <svg xmlns="http://www.w3.org/2000/svg" height="36" width="36" fill="none" viewBox="-4.004985 -7.600025 34.70987 45.60015"><path fill="#25F4EE" d="M10.4 11.9999v-1.2c-.4-.1-.8-.0999-1.2-.0999-5.1 0-9.2 4.0999-9.2 9.1999 0 3.1 1.6 5.9 3.9 7.5l-.1-.1C2.3 25.6999 1.5 23.5 1.5 21.2c0-5.1 4-9.1001 8.9-9.2001z"/><path fill="#25F4EE" d="M10.6002 25.4c2.3 0 4.1-1.8 4.2-4.1v-20h3.6c-.1-.4-.1-.8-.1-1.3h-5v20c-.1 2.2-1.9 4-4.2 4-.7 0-1.4-.2-1.9-.5.8 1.1 2 1.9 3.4 1.9zM25.3 8.1V6.9c-1.4 0-2.7-.4-3.8-1.1 1 1.1 2.3 2 3.8 2.3z"/><path fill="#FE2C55" d="M21.4999 5.8c-1.1-1.2-1.7-2.8-1.7-4.6h-1.4c.4 2 1.6 3.6 3.1 4.6zM9.2 15.5999c-2.3 0-4.2 1.9-4.2 4.2 0 1.6 1 3 2.3 3.7-.5-.7-.8-1.5-.8-2.4 0-2.3 1.9-4.2 4.2-4.2.4 0 .8.1 1.2.2v-5.1c-.4-.1-.8-.1-1.2-.1h-.2v3.8c-.5 0-.9-.1-1.3-.1z"/><path fill="#FE2C55" d="M25.2999 8.1001v3.8c-2.6 0-5-.8-6.9-2.2v10.2c0 5.1-4.1 9.2-9.2 9.2-2 0-3.8-.6-5.3-1.6 1.7 1.8 4.1 2.9 6.7 2.9 5.1 0 9.2-4.1 9.2-9.2v-10.2c2 1.4 4.4 2.2 6.9 2.2v-5c-.4 0-.9 0-1.4-.1z"/><path fill="#fff" d="M18.3999 19.8999v-10.2c2 1.4001 4.4 2.2 6.9 2.2v-3.9c-1.5-.3-2.8-1.1-3.8-2.2-1.6-1-2.7-2.7-3-4.6h-3.7V21.2c-.1 2.2-1.9 4-4.2 4-1.4 0-2.6-.7001-3.4-1.7001-1.3-.6-2.2-2-2.2-3.6 0-2.3 1.9-4.1999 4.2-4.1999.4 0 .8.0999 1.2.1999v-3.9c-5 .1-9 4.2001-9 9.2001 0 2.4.9 4.5999 2.5 6.2999 1.5 1 3.3 1.7001 5.3 1.7001 5.1-.1 9.2-4.3001 9.2-9.3001z"/></svg>          </a>
        )}
      </div>
    </footer>
  )
}

export const Footer = React.memo(FooterImpl)
